%
%     Relatorio do projeto
%
% Alexandre Machado - 43551
% João Rodrigues - 45802
% Dharmite Prabhudas - 45811
%
% Grupo 40
%
%
% O nosso projeto permite a leitura de um ficheiro de entrada .graph, que gera factos correspondentes e aplica
% um algoritmo a escolha(ucs,astar,...) com o nosso sucessor que esta implementado.
% É constituido por 4 ficheiro: 
% . procura_global_MP_40.pl - que é um ficheiro fornecido pelos docentes. Alterou-se a linha 52 de "objectivo(EI),"
%                             para "estado_final(EI),". Criamos o sucessor/3 e o sucessor/4 para serem usados por
%                             este código, juntamente com o perdicado estados_iguais/2, que verifica se dois estados são iguais;
% . readfile_40.pl          - um ficheiro que, recebendo o caminho para um ficheiro .graph, lê-o e cria factos;
% . writefile_40.pl         - um ficheiro que, recebendo um caminho, cria um ficheiro "solucoes.txt" com a solução possivel,
%                             imprimindo-a, também, na consola;
% . trabalho_40.pl          - que contem a excução do programa. Importando os outros ficheiros necessários, é este que chama os seus
%                             predicados, e que contem todos os predicados complementares do procura_global_MP_40
%
% O predicado le_fics(File) trata da conversão de um ficheiro .graph em factos
% do tipo edge(X,Y), estado_inicial([at(X,Y),...]) e estado_final([at(X,Y),...])
%
% Isto é feito tornando o ficheiro graph numa lista com cada elemento a corresponder
% a uma linha, e recursivamente iterar sobre cada uma e converter os numeros em factos
% sendo cada delineador do fim de a criação de um conjunto de factos (arcos ou estados)
% a string que as divide "ESTADO INICIAL:" e "ESTADO FINAL:"
%
% Para poder utilizar o predicado fornecido da procura/5 implementamos os seguintes predicados pedidos:
%
%           estados_iguais/2 :- Este predicado verifica indepentemente da posição de cada facto at(X,Y)
%           se os estados são iguais
%
%           sucessor/4 :- O predicado sucessor permite-nos devolver todas as possiveis jogadas dado um estado
%               Op: Devolve a Operação / Accção feita
%               EstadoAtual: O estado sobre a qual o sucessor vai calcular as possiveis jogadas
%               EstadoNovo: Devolve o estado resultado da operação feitas
%               1: O Custo de cada transição de estado.
%
%           link/2:- Verifica se existe uma ligação entre dois nós.
%
%           livre/3:- Prenchendo os campos de entrada com a informação, ele devolve se esta livre, e tambem
%           as casas possiveis se utilizado com todos os argumentos instanciados menos o de destino.
%
%           h/2: A heuristica está implementada utilizando o algoritmo de BFS para encontrar o custo que falta para o estado_final.
%
%
% Depois de realizada a pesquisa, o predicado cria_e_escreve_fic/3 como o nome indica
% cria e escreve um ficheiro com a solução.
%
% Isto tudo está englobado dentro da função main dentro do ficheiro projecto.pl
%  main/3 :- O primeiro input do predicado é o ficheiro graph, o segundo é o destino da solução e o terceiro é a estrategia utilizar.
%
%
% Exemplo de utilização:
%
%
% consult("./trabalho_40.pl").
% main("./ex1.graph", "./", ucs).
% 
% testado com o ucs, a-star e o gbf
%
% foram testados os ficheiros ex01.graph, fornecido pelos docentes, e todos os ficheiros fornecidos no test-simple
%
