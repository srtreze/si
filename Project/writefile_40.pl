
cria_e_escreve_fic(Path, L, Contador):-
  atom_string(Path, Path_St),
	atom_string("solutions.txt", Sol_St),
	string_concat(Path_St, Sol_St, S),
  open(S,write,Stream),
  ecrever_fic(Stream,L, Contador).

escreve_fic(Stream, [], Contador):-
  write(Stream, 'Contador: '),
  write(Stream,Contador),
  write('  '),
  write('Contador: '),
  write(Contador),
  close(Stream).

ecrever_fic(Stream, [X|L], Contador):-
  write('  '),
  ecrever_at(Stream, X, Contador, L).

ecrever_at(Stream, [], Contador, []):-
  nl(Stream), nl,
  escreve_fic(Stream, [], Contador).

ecrever_at(Stream, [], Contador, L):-
  nl(Stream), nl,
  ecrever_fic(Stream, L, Contador).

ecrever_at(Stream, [at(O,P)|X], Contador, L):-
  write(Stream, O),
  write(Stream, ':'),
  write(Stream, P),
  write(Stream, '  '),
  write(O),
  write(':'),
  write(P),
  write('  '),
  ecrever_at(Stream, X, Contador, L).
