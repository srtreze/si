% This buffer is for notes you don't want to save.
% If you want to create a file, visit that file with C-x C-f,
% then enter the text in that file's own buffer.

le_fics(File):-
  open(File, read, Str),
  read_file(Str,Lines),
  close(Str),
  criarFactos(Lines).


read_file(Stream,[]) :-
  at_end_of_stream(Stream).

read_file(Stream,[X|L]) :-
  \+ at_end_of_stream(Stream),
  read_line_to_codes(Stream,Codes),
  atom_chars(X, Codes),
  read_file(Stream,L), !.

criarFactos([]).

criarFactos([X|L]):-
  X=='ARCOS:',criarArcos(L).



criarArcos([X|L]):-
	X=='ESTADO INICIAL:', criarEstadoInicial(L,[]).

criarArcos([Z|L]):-
	atomic_list_concat([X,Y|_],' ',Z),
	atom_number(X,X2),
	atom_number(Y,Y2),
	assert(edge(X2,Y2)),
	criarArcos(L).



criarEstadoInicial([X|L],J):-
  X=='ESTADO FINAL:',assert(estado_inicial(J)),criarEstadoFinal(L,[]).

criarEstadoInicial([Z|L],J):-
  atomic_list_concat([X,Y|_],' ',Z),
  atom_number(X,X2),
  atom_number(Y,Y2),
  criarEstadoInicial(L,[at(X2,Y2)|J]).



criarEstadoFinal([],J):-
  assert(estado_final(J)),criarFactos([]).

criarEstadoFinal([Z|L],J):-
  atomic_list_concat([X,Y|_],' ',Z),
  atom_number(X,X2),
  atom_number(Y,Y2),
  criarEstadoFinal(L,[at(X2,Y2)|J]).

