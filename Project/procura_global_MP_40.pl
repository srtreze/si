% Procura global MP (melhor primeiro) com retrocesso
% Melhor de acordo com minimização de f = g + h
% 1) Caso geral: A*													(a-star)
% 2) Custo uniforme: h(X) = 0, para todos os X						(ucs)
% 3) Greedy Best-first: g(N) = 0, para todos os nós					(gbf)
% 4) Largura: f(N) = g(N), com g(N) = profundidade do Nó N			(bfs)
% 5) Profundidade: f(N) = g(N), com g(N) = -(profundidade do Nó N)	(dfsr)
% Na realidade, a implementação não concretiza f para os dois últimos

% Pressupostos:
% - Cada nó da árvore de procura é um quádruplo (F,G,[E|C],A), no qual:
%	. E representa o estado associado ao nó de procura,
%	. C representa o resto do caminho (ramo correspondente da árvore)
%	. A representa a sequência de acções (operadores) utilizados.
%	. F representa o valor da função f associado ao nó
%	. G representa o custo associado ao caminho/nó
%
% - O problema está definido de acordo com as regras, ou seja, têm que
% estar definidos os predicados:
%	. estado_inicial/1
%	. sucessor/4
%	. objectivo/1
%	. estados_iguais/2
%	. h/2 (opcional)

% procura/5
% procura(Est,E,C,A,Custo) - procura uma solução para o problema definido, usando a
% estratégia Est.
%
% Est - Estratégia (dfs, bfs, ...)
% E - Estado final encontrado
% C - Caminho encontrado (sequência de estados)
% A - Acções executadas (sequência de operadores)
% Custo - Custo Associado ao caminho encontrado
%
procura(Est,EstadoFinal,Caminho,Accoes,Custo) :-
	estado_inicial(E),
	valorH(Est,E,H),
	procura_global(Est,[(H,0,[E],[])],EstadoFinal,Caminho,Accoes,Custo).

% procura_global/6 - Procura global de acordo com estratégia
% procura_global(Estrategia,Fronteira,Estado,Caminho,Accoes,Custo)
%
% Estrategia - estrtégia a usar
% Fronteira - fronteira da árvore de procura (lista de nós pendentes)
% Estado - estado final encontrado
% Caminho - caminho encontrado
% Accoes - sequência de accões associada ao caminho encontrado
% Custo - custo do caminho encontrado.
%
procura_global(_,[(_,G,[E|C],A)|_],E,Caminho,Accoes,G) :-
	estado_final(EI),
	estados_iguais(E, EI),
	!,
	reverse([E|C],Caminho),
	reverse(A,Accoes).
procura_global(Est,Fronteira,R,Caminho,Accoes,Custo) :-
	expande_no(Est,Fronteira,NovaFronteira),
	!,
	procura_global(Est,NovaFronteira,R,Caminho,Accoes,Custo).

%%%%%%%% Expansão dos nós dependendo da estratégia

expande_no(Est,[(_,G,[E|C],A)|Resto],NovaFronteira) :-
	findall((F2,G2,[Estado,E|C],[Op|A]),descendente(Est,E,Estado,Op,C,G,F2,G2),L),
	actualiza_fronteira(Est,Resto,L,NovaFronteira).

actualiza_fronteira(bfs,Fronteira,NovosNos,NovaFronteira) :-
	append(Fronteira,NovosNos,NovaFronteira).

actualiza_fronteira(dfsr,Fronteira,NovosNos,NovaFronteira) :-
	append(NovosNos,Fronteira,NovaFronteira).

actualiza_fronteira(Estrategia,Fronteira,NovosNos,NovaFronteira) :-
	(Estrategia = ucs ; Estrategia = a-star ; Estrategia = gbf),
	append(Fronteira,NovosNos,NF),
	sort(NF,NovaFronteira).

descendente(Estrategia,EstadoOrigem,EstadoDestino,Operador,Caminho,G,NovoF,NovoG) :-
	sucessor(Operador,EstadoOrigem,EstadoDestino,Custo),
	\+ pertence_caminho(EstadoDestino,Caminho),
	NovoG is G + Custo,
	valorH(Estrategia,EstadoDestino,H),
%        NovoF is NovoG + H.
	(Estrategia = gbf, NovoF is H;
	 Estrategia \= gbf, NovoF is NovoG + H).

% valorH(Estrategia,Estado,Valor)
% Valor para a função heurística, em função da estratégia.
% A utilização de um algoritmo que use heuríticas pressupõe a definição
% do predicado h/2 na definição do problema.

valorH(Estrategia,E,V) :-
	(Estrategia = a-star ; Estrategia = gbf),
	!,
	h(E,V).
valorH(_,_,0).


%%%% Independentes da estratégia

% descendente/3 - sucessor evitando ciclos
% descendente(Estado, Sucessor, Operador, Caminho)
%
% Sucessor é o estado que resulta de aplicar o Operador a Estado,
% e não pertence ao Caminho (ramo da árvore) já percorrido.
descendente(E1,E2,Op,C) :-
	sucessor(Op,E1,E2),
	\+ pertence_caminho(E2,C).

% pertence_caminho/2 -
% pertence_caminho(E,C) -
%
% E é um estado que já pertence ao caminho C,
pertence_caminho(E,[X|_]) :-
	estados_iguais(E,X).
pertence_caminho(E,[_|C]) :-
	pertence_caminho(E,C).

%%%%%%%%%%%%
