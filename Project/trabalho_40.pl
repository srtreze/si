% importes

:- [readfile_40].

:- [writefile_40].

:- [procura_global_MP_40].

:- use_module(library(lists)).


%começa
main(File, Path_to_file, Est):-
	retractall(edge(_,_)),
	retractall(estado_inicial(_)),
	retractall(estado_final(_)),
	le_fics(File),
	estado_final(E),
	procura(Est, E, Caminho, _, Custo),
	cria_e_escreve_fic(Path_to_file, Caminho, Custo).


	%%%%%%%%%%%%%%%%%%
	 %              %
		%  Sucessor  %
	 %              %
	%%%%%%%%%%%%%%%%%%


%  sucessor/4
%
sucessor(Operacao,EstadoAtual,EstadoNove, 1):-
	moveUm(Operacao,EstadoAtual,EstadoNove,EstadoAtual).

%  sucessor/3
%
sucessor(Operacao,EstadoAtual,EstadoNove):-
	moveUm(Operacao,EstadoAtual,EstadoNove,EstadoAtual).

moveUm(Casa:CasaNova,[at(Objecto,Casa)|Resto],[at(Objecto,CasaNova)|Resto],EstadoAtual):-
	livre(Casa, CasaNova, EstadoAtual).

moveUm(Operacao,[at(Objecto,Casa)|Resto1],[at(Objecto,Casa)|Resto2],EstadoAtual):-
  moveUm(Operacao,Resto1,Resto2,EstadoAtual).


	 %%%%%%%%%%%%%%%%
	 %              %
	 %  Auxiliares  %
	 %              %
	 %%%%%%%%%%%%%%%%

livre(Casa, CasaNova, Estado):-
	(edge(Casa, CasaNova); edge(CasaNova, Casa)),
	not(member(at(_,CasaNova),Estado)).

% chegou ao fim, se:
estados_iguais([], []).
estados_iguais([At|Actual], Final):-
	member(At,Final),
	delete(Final, At, Final2),
	estados_iguais(Actual, Final2).


	%%%%%%%%%%%%%%%
	%             %
	%     BFS     %
	%             %
	%%%%%%%%%%%%%%%

%h_foreach([],Valor).

%h_foreach([E|T], Valor):-
	
	

h(EstadoAtual, Valor):-
	procura_bfs([([EstadoAtual],[])],E,Caminho),
	length(Caminho, I),
	Valor is I-1.

procura_bfs([([E|C],A)|_],E,Caminho) :-
	estado_final(EI),
	estados_iguais(E,EI),
	!,
	reverse([E|C],Caminho).
procura_bfs([([E|C],A)|Resto],R,Caminho) :-
	findall(([Estado,E|C],[Op|A]),descendente(E,Estado,Op,C),L), % L pode ser vazia
	!,
	append(Resto,L,NovaFronteira),
	procura_bfs(NovaFronteira,R,Caminho).








